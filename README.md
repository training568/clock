# clock

## Dev 手動開發並部署到 alpha env
* 簽到
    * git clone git@gitlab.com:training568/clock.git
    * cd clock
    * git branch -a
    * git checkout main
    * git branch feature-yourname
    * git checkout feature-yourname
    * git push -u origin feature-yourname
* 在 local 直接 build and test
    * 編譯執行檔
        * CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build -o clock-mac-amd64
        * CGO_ENABLED=0 GOOS=darwin GOARCH=arm64 go build -o clock-mac-arm64
        * CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -o clock-win-amd64
        * CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o clock-linux-amd64
        * 下載可在 local 跑的執行檔 (注意身份和權限)
            * gcloud init
            * gsutil cp gs://training/clock-os-arch clock
            * chmod +x clock
    * export LOG_OUTPUT=Stdout
    * ./clock
    * curl http://localhost:8080
* 在 local 用 docker 來測試
    * docker ps -a
    * docker images
    * cat Dockerfile
    * CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o clock
        * 下載 linux 執行檔 (注意身份和權限)
            * gcloud init
            * gsutil cp gs://training/clock-linux-amd64 clock
            * chmod +x clock
    * docker build -t clock:v0.1.0-yourname .
    * docker run -p 80:8080 --env LOG_OUTPUT=Stdout clock:v0.1.0-yourname
    * curl http://localhost
* docker push 到 GCP Artifact Registry (注意身份和權限)
    * gcloud init
    * gcloud auth configure-docker asia-east1-docker.pkg.dev
    * docker image tag clock:v0.1.0-yourname asia-east1-docker.pkg.dev/bccn-123456/training/clock:v0.1.0-yourname
    * docker push asia-east1-docker.pkg.dev/bccn-123456/training/clock:v0.1.0-yourname
* 修改 k8s-yaml files
    * image:tag
    * label and selector
    * env variables
* 用 kustomize 做出自己的 yaml 檔案
    * mkdir my-yaml
    * vi my-yaml/kustomization.yaml
    * ```
      apiVersion: kustomize.config.k8s.io/v1beta1
      kind: Kustomization

      commonLabels:
        app.kubernetes.io/name: yourname-clock

      namePrefix: yourname-

      bases:
        - ../k8s-yaml
      ```
    * kustomize build ./my-yaml
* 讓 kubectl 能在正確的 namespace 操作 (注意身份和權限)
    * gcloud init
    * gcloud container clusters get-credentials dev-k8s --zone=asia-east1
    * kubectl config set-context --current --namespace=alpha
    * kubectl get svc,deploy,po
    * kustomize build ./k8s-yaml
    * kustomize build ./my-yaml
    * kustomize build ./my-yaml | kubectl apply -f -
    * kubectl get svc,deploy,po
* 檢視 pod
    * kubectl get po -o wide
    * kubectl logs
    * kubectl run test-ubuntu --image=ubuntu --restart=Never -- sleep 1d
    * kubectl exec -it test-ubuntu -- bash
        * apt-get update
        * apt-get install curl
        * apt-get install iputils-ping
* git commit and push
    * rm clock
    * git add .
    * git commit -m "Upgrade to v0.1.0-yourname"
    * git push -u origin feature-yourname
* tag vX.Y.Z
    * git tag -l
    * git tag v0.1.0-yourname
    * git push origin v0.1.0-yourname

## Dev 手動打包 image 和 k8s-yaml 到 GCP Artifact Registry 和 Cloud Storage
* Image
    * docker image tag clock:v0.1.0-yourname asia-east1-docker.pkg.dev/bccn-123456/training/clock:v0.1.0-yourname
    * docker push asia-east1-docker.pkg.dev/bccn-123456/training/clock:v0.1.0-yourname
* Yaml
    * tar zcvf clock-yaml-v0.1.0-yourname.tar.gz k8s-yaml
    * gsutil cp clock-yaml-v0.1.0-yourname.tar.gz gs://training

## Ops 手動用 image 和 k8s-yaml 部署到 prod env
* gsutil cp gs://training/clock-yaml-v0.1.0-yourname.tar.gz clock-yaml-v0.1.0-yourname.tar.gz
* tar zxvf clock-yaml-v0.1.0-yourname.tar.gz
* kustomize build ./k8s-yaml | kubectl apply -f -
