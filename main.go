package main

import (
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	requestTotal = promauto.NewCounter(prometheus.CounterOpts{
		Name: "clock_request_total",
		Help: "The total number of request",
	})
)

func main() {

	router := mux.NewRouter()

	router.Path("/").HandlerFunc(handler)

	router.Path("/metrics").Handler(promhttp.Handler())

	log.Fatal(http.ListenAndServe(":8080", router))
}

func handler(w http.ResponseWriter, req *http.Request) {
	requestTotal.Inc()

	s := strconv.FormatInt(time.Now().UnixNano(), 10)

	if os.Getenv("LOG_OUTPUT") == "Stdout" {
		log.Print(s)
	}

	io.WriteString(w, s)
}
